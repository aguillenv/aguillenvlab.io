********
Topology
********

Google Cloud test environment
=============================

.. figure:: ../_static/_network/network_gcloud.png
   :width: 80%
   :alt: alternate text
   :figclass: align-center

Current architecture
====================

.. figure:: ../_static/_network/network_arch.png
   :width: 80%
   :alt: alternate text
   :figclass: align-center