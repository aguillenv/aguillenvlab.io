*********
Overview
*********

Chef is a powerful automation platform that transforms infrastructure into code. Whether it is operating in the cloud, on-premises,
or in a hybrid environment, Chef automates how infrastructure is configured, deployed, and managed across the network, no matter its size.

.. figure:: ../_static/_chef/chef_platform.png
   :width: 100%
   :alt: alternate text
   :figclass: align-center

   The chef platform architecture.

#. The **Chef Server** acts as a hub for configuration data. It stores cookbooks, the policies that are applied to nodes, and metadata that describes each registered node that is being managed by Chef.
#. The **Chef DK workstation** is the local device where users interact with Chef. On the workstation users write and test cookbooks and interact with the Chef server using the knife and chef command line tools.
#. The **Chef Client** nodes are the machines that are managed by Chef. The Chef client is installed on each node and is used to configure the node to its desired state. Nodes ask the Chef server for configuration details, such as recipes, templates, and file distributions.

Chef is a thin DSL (domain-specific language) built on top of Ruby. This approach allows Chef to provide just enough abstraction to make
reasoning about your infrastructure easy. Chef includes a built-in taxonomy of all the basic resources one might configure on a system,
plus a defined mechanism to extend that taxonomy using the full power of the Ruby language. Ruby was chosen because it provides the flexibility
to use both the simple built-in taxonomy, as well as being able to handle any customization path an organization requires.

Chef Provisioning
==================

Chef provisioning is a collection of resources that enable the creation of machines and machine infrastructures using the chef-client.
It has a plugin model that allows bootstrap operations to be done against any infrastructure. Chef provisioning is built around two major
components: the machine resource and drivers. In certain situations Chef provisioning will run multiple machine processes in-parallel, as long
as each of the individual machine resources have the same declared action. The machine_batch resource is used to run in-parallel processes.

A resource is a statement of configuration policy that:

- Describes the desired state for a configuration item
- Declares the steps needed to bring that item to the desired state
- Specifies a resource type—such as package, template, or service
- Lists additional details (also known as resource properties), as necessary
- Are grouped into recipes, which describe working configurations

There are multiple driver available for Chef provisioning, some of the most basic ones are:

- Amazon Web Services, a Chef provisioning driver for Amazon Web Services (AWS).
- Docker, a Chef provisioning driver for Docker.
- Fog, a Chef provisioning driver for Fog.
- Hanlon, a Chef provisioning driver for Hanlon.
- LXC, a Chef provisioning driver for LXC.
- Microsoft Azure, a Chef provisioning driver for Microsoft Azure.
- OpenNebula, a Chef provisioning driver for OpenNebula.
- SSH, a Chef provisioning driver for SSH.
- Vagrant, a Chef provisioning driver for Vagrant.
- vSphere, a Chef provisioning driver for VMware vSphere.

A driver-specific resource is a statement of configuration policy that:

- Describes the desired state for a configuration item that is created using Chef provisioning
- Declares the steps needed to bring that item to the desired state
- Specifies a resource type—such as package, template, or service
- Lists additional details (also known as properties), as necessary
- Are grouped into recipes, which describe working configurations

Security
=========

All communication with the Chef server must be authenticated using the Chef server API, which is a REST API that allows requests to be made
to the Chef server. Only authenticated requests will be authorized. Which are especially when using the knife plugin, the chef-client, or the 
Chef server web interface.

Authentication
---------------

The authentication process ensures the Chef server responds only to requests made by trusted users. Public key encryption is used by the Chef
server. When a node and/or a workstation is configured to run the chef-client, both public and private keys are created. The public key is stored
on the Chef server, while the private key is returned to the user for safe keeping. Both the chef-client and knife use the Chef server API when
communicating with the Chef server. The chef-validator uses the Chef server API, but only during the first chef-client run on a node. Each request
to the Chef server from those executables sign a special group of HTTP headers with the private key. The Chef server then uses the public key to
verify the headers and verify the contents.

- **chef-validator**, every request made by the chef-client to the Chef server must be an authenticated request using the Chef server API and a private key. When the chef-client makes a request to the Chef server, the chef-client authenticates each request using a private key, which should be stored safely.
- **Knife**, RSA public key-pairs are used to authenticate knife with the Chef server every time knife attempts to access the Chef server. This ensures that each instance of knife is properly registered with the Chef server and that only trusted users can make changes to the data.
- **Web Interface**, the Chef server user interface uses the Chef server API to perform most operations. This ensures that authentication requests to the Chef server are authorized. This authentication process is handled automatically and is not something that users of the hosted Chef server will need to manage. For the on-premises Chef server, the authentication keys used by the web interface are maintained by the individual administrators who are responsible for managing the server [#]_.

Authorization
--------------

The Chef server uses a role-based access control (RBAC) model to ensure that users may only perform authorized actions. The Chef server uses role-based
access control (RBAC) to restrict access to objects—nodes, environments, roles, data bags, cookbooks, and so on. This ensures that only authorized user
and/or chef-client requests to the Chef server are allowed. Access to objects on the Chef server is fine-grained, allowing access to be defined by object
type, object, group, user, and organization. The Chef server uses permissions to define how a user may interact with an object, after they have been
authorized to do so.

Public and Private Keys
------------------------

The authentication process ensures the Chef server responds only to requests made by trusted users. Public key encryption is used by the Chef server.
When a node and/or a workstation is configured to run the chef-client, both public and private keys are created. The public key is stored on the Chef server,
while the private key is returned to the user for safe keeping. Both the chef-client and knife use the Chef server API when communicating with the Chef server.
The chef-validator uses the Chef server API, but only during the first chef-client run on a node. Each request to the Chef server from those executables sign
a special group of HTTP headers with the private key. The Chef server then uses the public key to verify the headers and verify the contents.

SSL Certificates
-----------------

Chef server 12 enables SSL verification by default for all requests made to the server, such as those made by knife and the chef-client. The certificate that
is generated during the installation of the Chef server is self-signed, which means the certificate is not signed by a trusted certificate authority (CA) that
ships with the chef-client. The certificate generated by the Chef server must be downloaded to any machine from which knife and/or the chef-client will make
requests to the Chef server.


Run in FIPS Mode
-----------------

Federal Information Processing Standards (FIPS) is a United States government computer security standard that specifies security requirements for cryptography.
The current version of the standard is FIPS 140-2. The chef-client can be configured to allow OpenSSL to enforce FIPS-validated security during a chef-client run.
This will disable cryptography that is explicitly disallowed in FIPS-validated software, including certain ciphers and hashing algorithms. Any attempt to use any
disallowed cryptography will cause the chef-client to throw an exception during a chef-client run.

.. [#] Security features available in Chef server: https://docs.chef.io/server_security.html
