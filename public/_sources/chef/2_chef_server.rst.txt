************
Chef-Server
************

The Chef server acts as a hub of information. Cookbooks and policy settings are uploaded to the Chef server by users from
workstations.

Chef management console is the user interface for the Chef server. It is used to manage data bags, attributes, run-lists,
roles, environments, and cookbooks, and also to configure role-based access for users and groups. The Chef Server has some
features which are:

- **Search**: Search indexes allow queries to be made for any type of data that is indexed by the Chef server.
- **Manage**: Chef management console is a web-based interface for the Chef server that provides users a way to manage.
- **Data Bag**: A data bag is a global variable that is stored as JSON data and is indexed for searching and can be loaded by a recipe or accessed during a search.
- **Policy**: Policy defines how business and operational requirements, processes, and production workflows map to objects that are stored on the Chef server.

Chef Server Components
=======================

The front-end for the Chef server is written using Erlang.The Chef server can scale to the size of any enterprise. The diagram
shows the various components that are part of a Chef server deployment and how they relate to one another.

.. figure:: ../_static/_chef/chef_components.png
   :width: 100%
   :alt: alternate text
   :figclass: align-center
	
   The chef server components.

Scaling the Chef Server
------------------------

The Chef server itself is highly scalable. A single virtual machine running the Chef server can handle requests for many thousands
of nodes. As the scale increases, it’s a straightforward process to expand into a tiered front-end, back-end architecture with
horizontally scaled front-ends to relieve pressure on system bottlenecks. That said, it’s best to isolate failure domains with
their own Chef server, rather than trying to run every node in an infrastructure from a single central, monolithic Chef server
instance/cluster. For instance, if there are West coast and East coast data centers, it is best to have one Chef server instance
in each datacenter. Deploys to each Chef server can be synchronized upstream by CI software. The primary limiting bottleneck for
Chef server installations is almost always input/output operations per second (IOPS) performance for the database filesystem.
The key unit of measure for scaling the Chef server is the number of chef-client that runs per minute: CCRs/min. Typically, the
Chef server does not require a high availability or tiered topology until the number of CCRs/min is higher than 333/min (approximately
10k nodes).

.. figure:: ../_static/_chef/chef_high_availability.png
   :width: 100%
   :alt: alternate text
   :figclass: align-center

   The chef server high availability structure.

Chef-Server standalone System Requirements
============================================

All machines in a Chef server deployment have the following hardware requirements. Disk space for standalone and backend servers
should scale up with the number of nodes that the servers are managing. A good rule to follow is to allocate 2 MB per node. The disk
values listed below should be a good default value that you will want to modify later if/when your node count grows. Fast, redundant
storage (SSD/RAID-based solution either on-prem or in a cloud environment) is preferred.

- For *all* deployments.

  - 64-bit architecture

- For a *standalone* deployment.

  - 4 total cores (physical or virtual)
  - 8 GB of RAM or more
  - 5 GB of free disk space in /opt
  - 5 GB of free disk space in /var
		
Before installing the Chef server, ensure that each machine has the following installed and configured properly:

- **Hostnames** — Ensure that all systems have properly configured hostnames. The hostname for the Chef server must be a FQDN or public IP address, including the domain suffix, and must be resolvable.
- **NTP** — Ensure that every server is connected to NTP; the Chef server is sensitive to clock drift
- **Mail Relay** — The Chef server uses email to send notifications for various events; a local mail transfer agent should be installed and available to the Chef server
- **cron** — Periodic maintenance tasks are performed using cron
- **git** — git must be installed so that various internal services can confirm revisions
- **libfreetype and libpng** — These libraries are required
- **Apache Qpid** — This daemon must be disabled on CentOS and Red Hat systems
- **Required users** — If the environment in which the Chef server will run has restrictions on the creation of local user and group accounts, ensure that the correct users and groups exist before reconfiguring
- **Firewalls and ports** — If host-based firewalls (iptables, ufw, etc.) are being used, ensure that ports 80 and 443 are open. These ports are used by the nginx service

Chef-Server Installation & Configuration [#]_ [#]_
==================================================

The standalone installation of Chef server creates a working installation on a single server. Standalone is useful when it runs in a
virtual machine, for proof-of-concept deployments, or as a part of a development or testing loop. This installation is on a virtual
machine running Ubuntu 18.04 LTS.

First step is to download the package from the Chef official site [#]_, using the wget command (may need to be installed if missing from the system).

.. code-block:: shell

   $ wget https://packages.chef.io/files/stable/chef-server/12.17.33/ubuntu/16.04/chef-server-core_12.17.33-1_amd64.deb

As a root user, install the Chef server package on the server, using the name of the package provided by Chef with the following command.
Next the package can be removed from the system.

.. code-block:: shell

   $ sudo dpkg -i chef-server-core_12.17.33-1_amd64.deb
   $ rm chef-server-core_12.17.33-1_amd64.deb

To start all of the services it is needed to run the following command, because the Chef server is composed of many different services that
work together to create a functioning system, this step may take a few minutes to complete.
	
.. code-block:: shell

   $ sudo chef-server-ctl reconfigure
	
To create an administrator run the following command which will generate a RSA private key. This is the user’s private key and should be
saved to a safe location. The *--filename* option will save the RSA private key to the specified absolute path.

.. code-block:: shell

   $ sudo chef-server-ctl user-create USER_NAME FIRST_NAME LAST_NAME EMAIL 'PASSWORD' --filename /PATH/FILE_NAME.pem
	
To create an organization run the following command.
	
.. code-block:: shell

   $ sudo chef-server-ctl org-create short_name 'full_organization_name' --association_user user_name --filename /PATH/ORGANIZATION-validator.pem

The name must begin with a lower-case letter or digit, may only contain lower-case letters, digits, hyphens, and underscores, and must be
between 1 and 255 characters. The full name must begin with a non-white space character and must be between 1 and 1023 characters. The *--association_user*
option will associate the user_name with the admins security group on the Chef server. An RSA private key is generated automatically. This is the
chef-validator key and should be saved to a safe location. The --filename option will save the RSA private key to the specified absolute path.

Use Chef management console to manage data bags, attributes, run-lists, roles, environments, and cookbooks from a web user interface. On the Chef server,
run the following commands to install the chef manage, reconfigure the chef server and then reconfigure the chef-manage.

.. note::

   Include *--accept-license* to automatically accept the license.

.. code-block:: shell

   $ sudo chef-server-ctl install chef-manage
   $ sudo chef-server-ctl reconfigure
   $ sudo chef-manage-ctl reconfigure --accept-license

.. warning::

   In case that changes have been applied to the chef server than the server must be reconfigured again for them to be taken into consideration. The command to do this is the following.

   .. code-block:: shell

      $ sudo chef-server-ctl reconfigure

Now the server is installed and ready and can be accessed through the https://Server_IP_or_functioning_hostname

.. [#] Chef Server installation: https://www.youtube.com/watch?v=91o60zngIrg&list=WL&index=72
.. [#] Chef Server installation procedure: https://docs.chef.io/install_server.html#standalone
.. [#] Chef Server package: https://downloads.chef.io/chef-server/12.17.33
