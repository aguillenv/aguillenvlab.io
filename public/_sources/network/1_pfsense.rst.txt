********************
PfSense installation
********************

#. Download USB Memstick image from `PfSense official website <https://www.pfsense.org/download/>`_.
#. Follow these `instructions <https://blog.kylemanna.com/cloud/pfsense-on-google-cloud/>`_.