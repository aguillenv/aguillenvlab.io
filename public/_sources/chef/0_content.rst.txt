#####################
Chef for Provisioning
#####################



.. contents::
   :local:




.. include:: 1_overview.rst
.. include:: 2_chef_server.rst
.. include:: 3_chef_workstation.rst
.. include:: 4_chef_client.rst
.. include:: 5_basic_usage.rst
.. include:: 6_first_recipe.rst
.. include:: 7_glossary.rst



.. [#] Chef full documentation: https://docs.chef.io/
