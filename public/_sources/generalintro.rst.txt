############
Introduction
############

.. contents::
   :depth: 1
   :local:

.. highlight:: console

************
Provisioning
************

Provisioning is "the process of preparing and equipping a network to allow it to 
provide new services to its users." More precisely, when we're talking of server 
provisioning, we're talking about a "set of actions to prepare a server with appropriate 
systems, data and software, and make it ready for network operation." [#]_.

*******
Purpose
*******

The purpose of the Provisioning Environment project is to create a pentesting lab. 

A **pentesting lab** is an isolated-from-network environment that provides vulnerable systems that can be used to test and understand vulnerabilities.  
As long as the systems will be targets of malicious attacks, they will be most likely compromised and so, from a certain point, not further available or usable.

Therefore, a provisioning system can be used to automatically manage and restore the original state of the machine, for instance by fixing some compromised libraries or, in the worst case scenario, by installing the operative system by scratch.     

For these project, some of the most well-known provisioning system will be used and tested:

- Ansible_
- Chef_
- Puppet_
- SaltStack_
 


.. _Ansible: https://www.ansible.com/
.. _Chef: https://www.chef.io/chef/
.. _Puppet: https://puppet.com/
.. _SaltStack: https://saltstack.com/
 


.. [#] Wikipedia.org - Provisioning: https://en.wikipedia.org/wiki/Provisioning_(telecommunications\)   
