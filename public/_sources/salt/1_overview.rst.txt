********
Overview
********

Salt is a **configuration management system**, capable of maintaining remote nodes 
in defined states (for example, ensuring that specific packages are installed 
and specific services are running) [#]_. The configuration management system is 
called in the Saltstack as **Salt master**, and the remote nodes are called 
**Salt minions**.

.. figure:: ../_static/_salt/salt_master_minion.png
   :width: 30%
   :alt: alternate text
   :figclass: align-center

Publish-subscribe pattern
=========================

Salt approach to infrastructure management is based in the `publish-subscribe <https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern>`_ 
pattern. The important thing is that the Salt master can manage all nodes as 
equal or can have different configuration targetting specific nodes or group of 
nodes.

.. figure:: ../_static/_salt/salt_basic_comm.png
   :width: 30%
   :alt: alternate text
   :figclass: align-center

Independency
============

The **Salt master** does not need to do anything of the work, just send a set of 
instructions and properties, the **Salt minions** that match with this information will 
execute the commands and send the results back to the master. All minions receive 
the information simultaneously and work in parallel.

Security
========

As a matter of security, Salt has different approaches:

- All the communication between master and minion is **encrypted** using AES keys.
- It counts with a rotation keys policy for the AES key used to encrypt the jobs 
  that are sent to the Salt minions. This key is generated each time that the Salt 
  master restarts and when the Salt minion key is deleted.
- With the **user access control** before sending commands to the minions, Salt 
  check if the user executing the task has all the privileges necessaries.
- To start the communication between master and minion, the minion sent its 
  **public key** to the master in the network and the master needs to accept that 
  **subscriber**.

.. figure:: ../_static/_salt/salt_public_key.png
   :width: 30%
   :alt: alternate text
   :figclass: align-center

Modular - Plugin oriented
=========================

The Salt's core is really lightweight as it is but has the capability to add the 
necessary subsystems and plugins to delegate the work to perform different tasks.

.. figure:: ../_static/_salt/salt_subsystems.png
   :width: 70%
   :alt: alternate text
   :figclass: align-center

Cross-platform
==============

Salt has the capability to abstract the details of the target OS, hardware or 
systems tools so it can normalize and run all commands on different platforms. 
Also, Salt runs almost everywhere Python runs, and if the system does not support 
Python it offers an option call Salt proxy minions.

Scalable
========

With all the characteristics above and the way how Salt is designed makes it 
easy to scale. **Only one Salt master is capable to manage up to 10.000 minions** 
**without any problem.**

**For more detailed information go to** `Understanding SaltStack: Get started tutroial <https://docs.saltstack.com/en/getstarted/system/index.html>`_.
